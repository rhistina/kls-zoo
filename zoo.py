from animals import *
import sys

class Zoo:
    def __init__(self,name):
        self.name = name
        self.animals = []

    """
        get_num_animals
        Returns a message stating the number of animals at this zoo
    """
    def get_num_animals(self):
        return "There's {} animals at this zoo.".format(len(self.animals))

    """
        list_animals
        Returns a formatted string of the listed animals
    """
    def list_animals(self):

        formated_animal_string = ""
        if self.animals:
            for i,animal in  enumerate(self.animals):
                formated_animal_string += "{} is located at enclosure # {}\n".format(animal.name(), i)
        else:
            return "There's no animals at this zoo"

        return formated_animal_string

    """
        visit_animal
        Returns a formatted message of action which comprises of 1) visiting the animal at it's enclosure,
        2) stating some fun facts, 3) the sound an animal would make when you get too close
    """
    def visit_animal(self, index):
        index = int(index)

        if not self.animals:
            return "There's currently no animals being showcased at this zoo"

        if index not in range(len(self.animals)):
            return "You've selected an invalid enclosure ID."


        info_message = ""
        for i,animal in enumerate(self.animals):
            if index == i:
                info_message += ("Visiting {} at their enclosure {}.\n".format(animal.name(), i))
                info_message += ("The {} belongs to the class \'{}\', is {}, and has a {} diet.\n".format(animal.name(), animal.type(), animal.blood_type(), animal.diet(),))
                info_message += ("Get closer to the glass enclosure to hear them make noise! \n")
                info_message += (animal.make_sound())

        return info_message

class MyZoo:
    def __init__(self):
        self.zoo = Zoo("Rhistina's Zoo")
        self.zoo.animals = [Mammal(name="Wolf", sound="howl", diet="carnivore"),
                            Mammal(name="Zebra", sound="neigh", diet="herbivore"),
                            Mammal(name="Lion", sound="roar", diet="carnivore"),
                            Mammal(name="Chimpanzee", sound="grunt", diet="omnivore"),
                            Reptile(name="Snake", sound="hiss", diet="herbivore"),
                            Reptile(name="Crocodile", sound="growl", diet="carnivore"),
                            Bird(name="Crow", sound="caw", diet="omnivore"),
                            Bird(name="Owl", sound="screech", diet="carnivore"),
                            Fish(name="Salmon", sound=None, diet="herbivore"),
                            Amphibian("Frog", sound="ribbit", diet="carnivore"),
                            Arthropod("Spider", sound=None, diet="carnivore")]
        self.options = {'1': self.zoo.get_num_animals,
                        '2': self.zoo.list_animals,
                        '3': self.zoo.visit_animal,
                        '4': self.exit}

    def enter(self):
        print ("Welcome to {}".format(self.zoo.name))
        while True:
            print ("\nWhat would you like to do?\n")
            print ("--- 1. How many animals?")
            print ("--- 2. List animals at this zoo")
            print ("--- 3. Visit an animal at its enclosure ")
            print ("--- 4. Quit")

            input_correct = False

            while not input_correct:
                choice = input()
                if choice in ['1','2','3','4']:
                    input_correct = True
                    index = ""
                    choice_result = ""
                    if choice == "3":
                        print ("Enter the enclosure ID you want to visit")
                        index = input()
                        choice_result = self.options[choice](index)
                    else:
                        choice_result = self.options[choice]()

                    print (choice_result)
                else:
                    print ("Wrong input. Select an option from above.")

    def exit(self):
        print ("Thanks for visiting {}!".format(self.zoo.name))
        sys.exit()

if __name__ == "__main__":
    MyZoo().enter()
