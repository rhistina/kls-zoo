# KLS-ZOO #


### Description ###

A Python 3.5 implementation of a zoo to showcase inheritance. 

* The Animal class is the base-class. 
* The Mammal, Reptile, Fish, Bird, Arthropod, and Amphibian classes are sub-classes that inherit behavior from Animal.
* Fish and Arthropod sub-classes showcase method overriding  

### Getting Started ###

To execute the program, simply run below in command line:
```
#!python

python zoo.py

```

#### Sample Execution ###
```
#!python
>>> python zoo.py
Welcome to Rhistina's Zoo

What would you like to do?

--- 1. How many animals?
--- 2. List animals at this zoo
--- 3. Visit an animal at its enclosure 
--- 4. Quit
1
There's 11 animals at this zoo.

What would you like to do?

--- 1. How many animals?
--- 2. List animals at this zoo
--- 3. Visit an animal at its enclosure 
--- 4. Quit
2
Wolf is located at enclosure # 0
Zebra is located at enclosure # 1
Lion is located at enclosure # 2
Chimpanzee is located at enclosure # 3
Snake is located at enclosure # 4
Crocodile is located at enclosure # 5
Crow is located at enclosure # 6
Owl is located at enclosure # 7
Salmon is located at enclosure # 8
Frog is located at enclosure # 9
Spider is located at enclosure # 10


What would you like to do?

--- 1. How many animals?
--- 2. List animals at this zoo
--- 3. Visit an animal at its enclosure 
--- 4. Quit
3
Enter the enclosure ID you want to visit
3
Visiting Chimpanzee at their enclosure 3.
The Chimpanzee belongs to the class 'mammal', is warm blooded, and has a omnivore diet.
Get closer to the glass enclosure to hear them make noise! 
**The chimpanzee grunts at you!**


What would you like to do?

--- 1. How many animals?
--- 2. List animals at this zoo
--- 3. Visit an animal at its enclosure 
--- 4. Quit
4
Thanks for visiting Rhistina's Zoo!
```


### Contact ###

* Rhistina Revilla (rhistina@gmail.com)