class Animal(object):
    def __init__(self, name, sound, diet):
        self._name = name
        self._sound = sound
        self._diet = diet


    def name(self):
        return self._name


    def sound(self):
        return self._sound


    def diet (self):
        return self._diet


    def make_sound(self):
        if self.sound()[-1] in ['s','ch']:
            x = "es"
        else:
            x = "s"
        return ("**The {} {}{} at you!**\n".format(self._name.lower(), self.sound().lower(), x))


class Mammal(Animal):
    def type (self):
        return "mammal"


    def blood_type(self):
        return "warm blooded"


class Reptile(Animal):
    def type(self):
        return "reptile"


    def blood_type(self):
        return "cold blooded"


class Bird(Animal):
    def type(self):
        return "bird"


    def blood_type(self):
        return "warm blooded"


class Fish(Animal):
    def type (self):
        return "fish"


    def blood_type(self):
        return "cold blooded"


    def make_sound(self):
        return "Animals in class \'{}\' make no sound.".format(self.type())


class Amphibian(Animal):
    def type (self):
        return "amphibian"


    def blood_type(self):
        return "cold blooded"


class Arthropod(Animal):
    def type (self):
        return "arthropod"


    def blood_type(self):
        return "cold blooded"


    def make_sound(self):
        return "Animals in class \'{}\' make no sound.".format(self.type())
